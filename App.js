import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { Ionicons } from '@expo/vector-icons'
import { useSafeArea } from 'react-native-safe-area-context';
import { View } from 'react-native'
import Index from "./src/css/index"
import './src/css/index'
import Today from './src/view/today'
import Game from './src/view/game'
import Search from './src/view/serach'
import AppStore from './src/view/appstore'
import AppDetails from './src/components/app-details'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const TopTab = createMaterialTopTabNavigator()

const Tabs = () => {
  return <Tab.Navigator>
    <Tab.Screen name="Today" component={Today} options={{
      tabBarIcon: ({ color, size }) => (
        <Ionicons name="ios-today" color={color} size={size} />
      )
    }}/>
    <Tab.Screen name="Game" component={TopTabs} options={{
      tabBarIcon: ({ color, size }) => (
        <Ionicons name="ios-rocket" color={color} size={size} />
      )
    }}/>
    <Tab.Screen name="App" component={AppStore} options={{
      tabBarIcon: ({ color, size }) => (
        <Ionicons name="ios-apps" color={color} size={size} />
      )
    }}/>
    <Tab.Screen name="Search" component={Search} options={{
      tabBarIcon: ({ color, size }) => (
        <Ionicons name="ios-search" color={color} size={size} />
      )
    }}/>
  </Tab.Navigator>
}

const TopTabs = () => {
  const insets = useSafeArea()
  return <View style={{flex: 1, paddingTop: insets.top, backgroundColor: Index.game_topbar_safeview}}>
    <TopTab.Navigator 
      tabBarOptions={{
        style: {
          backgroundColor: Index.game_topbar_background
        },
        inactiveTintColor: Index.game_init_text_color,
        activeTintColor: Index.game_active_text_color,
        indicatorStyle: {
          backgroundColor: Index.game_icon_color
        }
      }}>
      <TopTab.Screen name="Game" component={Game}/>
      <TopTab.Screen name="Search" component={Search}/>
    </TopTab.Navigator>
  </View>
}

const App = () => {
  return <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen 
      name="app" 
      component={Tabs}
      options={{headerShown: false}}
      />
      <Stack.Screen 
        name="details" 
        component={AppDetails}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  </NavigationContainer>
}

export default () => {
  return <App />
};