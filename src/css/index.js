export default {

  // tabar color size
  tabar_color: "#8E8F8F",
  tabar_active_color: "#50A3FC",
  tabar_icon_size: 20,

  // page background
  page_background: "",

  // game topbar bcakground
  game_topbar_safeview: "black",
  game_topbar_background: "black",
  game_init_text_color: "white",
  game_active_text_color: "#C08B65",
  game_icon_color: "#C08B65",

  // padding
  padding_left: 20

}