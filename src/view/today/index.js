import React from 'react'
import { SafeAreaView, StyleSheet, View, FlatList } from 'react-native'
import AppTop from '../../components/app-top'
import AppItem from '../../components/app-item'

const item = {
  topic: "Today"
}

const dataItem = [
  {
    type: "normal",
    image: require('../../assets/image/swiperItem.png'),
    label: "生活解决方案",
    labelColor: "white",
    topic: "控制不住情绪怎么办？",
    topicColor: "white"
  },
  {
    type: "swiper",
    image: require('../../assets/image/app4.png'),
    label: "搞点小发明",
    labelColor: "black",
    topic: "轻松设置复杂密码",
    topicColor: "black"
  },
  {
    type: "store",
    image: require('../../assets/image/app1.png'),
    label: "今日最热hot",
    labelColor: "white",
    topic: "三国系列传奇色彩",
    topicColor: "white"
  },
  {
    type: "storebatch",
    image: require('../../assets/image/app1.png'),
    label: "尝鲜",
    labelColor: "#a8a8a8",
    topic: "对焦就能拍，拍完不用改",
    topicColor: "black",
    storebatch: [
      {id: 1, label: "极致体验", topic: "INCH 相机", labeColor: "", topicColor: "", image: require('../../assets/image/CHIC.png')},
      {id: 2, label: "极间相机，真实胶片🎞️", topic: "NO MO 相机 - 你的拍立得", labeColor: "", topicColor: "", image: require('../../assets/image/NOMO.png')},
      {id: 3, label: "真胶片，不浮夸", topic: "做后一卷胶片", labeColor: "", topicColor: "", image: require('../../assets/image/DAZZ.png')},
      {id: 4, label: "还原真实胶卷体验", topic: "FIMO - 复古胶片相机", labeColor: "", topicColor: "", image: require('../../assets/image/OO.png')}
    ]
  }
]

const Today = () => {
  return <>
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.toady}>
      <FlatList
        ListHeaderComponent={() => <AppTop item={item} />}
        data={dataItem}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => <AppItem item={item} />}
        keyExtractor={(item) => item.type}
      />
      </View>
    </SafeAreaView>
  </>
}

const styles = StyleSheet.create({
  toady: {
    paddingHorizontal: 18,
    flex: 1
  }
})
export default Today