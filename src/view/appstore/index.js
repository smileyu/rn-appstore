import React from 'react'
import { View, Text, StyleSheet, SafeAreaView, FlatList, ScrollView } from 'react-native'
import StoreItem from '../../components/store-item'

const AppStore = () => {
  return <>
    <SafeAreaView style={{flex: 1, backgroundColor: "white"}}>
      <View style={styles.store}>
       <StoreItem />
      </View>
    </SafeAreaView>
  </>
}
const styles = StyleSheet.create({
  store: {
    flex: 1,
    paddingHorizontal: 18,

  }
})
export default AppStore