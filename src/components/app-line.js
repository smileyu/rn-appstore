import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const AppLine = ({style}) => {
  return <>
    <View style={[styles.border_line, style]}></View>
  </>
}
const styles = StyleSheet.create({
  border_line: {
    width:"100%", 
    height: 0.5, 
    backgroundColor: "#A8A8A8"
  },
})
export default AppLine