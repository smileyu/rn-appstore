import React from 'react'
import { View, Text, StyleSheet, ImageBackground, Dimensions, Image, TouchableOpacity, FlatList, Modal } from 'react-native'
import AppPush from './app-push'
import AppLine from './app-line'
import { useNavigation } from '@react-navigation/native';
const {height} = Dimensions.get("window")

// const navigation = () => {
//   return useNavigation();
// } 

{/* 普通推荐item */ }
const normal = (item) => {
  const navigation = useNavigation();
  return <>
    <TouchableOpacity activeOpacity={.8} onPress={() => navigation.navigate("details")}>
    <ImageBackground source={item.image} imageStyle={styles.normal_img_bk} style={styles.normal_item}>
      <View style={styles.app_item_text}>
        <Text style={[styles.app_item_label, {color: item.labelColor}]}>{item.label}</Text>
        <Text style={[styles.app_item_topic, {color: item.topicColor}]}>{item.topic}</Text>
      </View>
    </ImageBackground>
    </TouchableOpacity>
  </>
}

{/* 轮播推荐item */ }
const swiper = item => {
  return <>
    <View style={[styles.swiper_item, styles.app_item_shadow]}>
      <View style={styles.app_item_text}>
        <Text style={[styles.app_item_label, {color: item.labelColor}]}>{item.label}</Text>
        <Text style={[styles.app_item_topic, {color: item.topicColor}]}>{item.topic}</Text>
      </View>
      <Image source={item.image} 
      style={styles.swiper_item_image} />
    </View>
  </>
}

{/* 直接推荐item */ }
const store = item => {
  return <>
    <TouchableOpacity activeOpacity={.8}>
      <View style={[styles.store_item, styles.app_item_shadow]}>
        <ImageBackground source={item.image} imageStyle={styles.store_img_bk} style={styles.store_item_image}>
          <View style={styles.app_item_text}>
            <Text style={[styles.app_item_label, {color: item.labelColor}]}>{item.label}</Text>
            <Text style={[styles.app_item_topic, {color: item.topicColor}]}>{item.topic}</Text>
          </View>
        </ImageBackground>
        <View style={[{marginTop: 5}, styles.padding_horizontal]}>
          <AppPush item={item} />
        </View>
      </View>
    </TouchableOpacity>
  </>
}

const style = {
  marginLeft: 60
}

{/* 批量推荐 */}
const storebatch = item => {
  return <>
    <View style={[styles.store_item, styles.app_item_shadow]}>
      <View style={styles.app_item_text}>
        <Text style={[styles.app_item_label, {color: item.labelColor}]}>{item.label}</Text>
        <Text style={[styles.app_item_topic, {color: item.topicColor}]}>{item.topic}</Text>
      </View>
      <FlatList style={styles.padding_horizontal} 
        data={item.storebatch} 
        ItemSeparatorComponent={() => <AppLine style={style} />}
        keyExtractor={(item, index) => `${index}`} 
        renderItem={({item}) => <>
          <AppPush item={item}/>
        </>}/>
    </View>
  </>
}

const AppItem = ( {item} ) => {
  switch (item.type) {
    case "normal": return normal(item)
    case "swiper": return swiper(item)
    case "store": return store(item)
    case "storebatch": return storebatch(item)
    default: return normal(item)
  }
}

const styles = StyleSheet.create({
  app_item_shadow: {
    elevation: 20,
    shadowOffset: {width: 0, height: 0},
    shadowColor: '#A8A8A8',
    shadowOpacity: 0.2,
  },
  normal_item: {
    height: 0.5 * height,
    width: "100%",
    marginBottom: 15
  },
  swiper_item: {
    height: 0.4 * height,
    width: "100%",
    backgroundColor: "white",
    borderRadius: 10,
    marginBottom: 16
  },
  store_item: {
    width: "100%", 
    height: 0.38 * height, 
    backgroundColor: "white", 
    borderRadius: 10, 
    marginBottom: 15
  },
  swiper_item_image: {
    width: "100%", 
    height: 0.3 * height, 
    borderBottomRightRadius: 10, 
    borderBottomLeftRadius: 10
  },
  store_item_image: {
    width: "100%",
    height: 0.3 * height, 
    borderTopRightRadius: 10, 
    borderTopLeftRadius: 10
  },
  normal_img_bk: {
    borderRadius: 10
  },
  store_img_bk: {
    borderTopRightRadius: 10, 
    borderTopLeftRadius: 10
  },
  app_item_text: {
    padding: 15
  },
  app_item_label: {
    fontWeight: "bold"
  },
  app_item_topic: {
    fontSize: 28,
    marginVertical: 8,
    fontWeight: "bold"
  },
  padding_horizontal: {
    paddingHorizontal: 20
  }
})
export default AppItem