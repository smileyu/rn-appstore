import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'

const AppPush = ({item}) => {
  return <>
    <View style={styles.store_push}>
      {/* app介绍 */}
      <View style={styles.store_push_app}>
        <Image source={item.image} style={styles.store_push_image} />
        <View style={styles.store_push_app_text_view}>
          <Text style={styles.store_push_text_label}>{item.topic}</Text>
          <Text style={styles.store_push_text_topic}>{item.label}</Text>
        </View>
      </View>
      {/* 获取下载 */}
      <View style={styles.store_down}>
        <TouchableOpacity style={styles.store_down_text_view}>
          <Text style={styles.store_down_topic}>获取</Text>
        </TouchableOpacity>
        <Text style={styles.store_down_label}>App内购买</Text>
      </View> 
    </View>
  </>
}
const styles = StyleSheet.create({
  store_push: {
    alignItems: 'center', 
    flexDirection: "row",
    marginVertical: 5,
    justifyContent: "space-between"
  },
  store_push_app: {
    flexDirection: "row", 
    alignItems: "center"
  },
  store_push_image: {
    width: 50, 
    height: 50, 
    borderRadius: 8,
    borderColor: "#f1f1f1",
    borderWidth: 0.5
  },
  store_push_app_text_view: {
    flexDirection: "column",
     marginLeft: 13
  },
  store_push_text_label: {
    fontWeight: "bold"
  },
  store_push_text_topic: {
    color: "#A8A8A8", 
    fontSize: 12, 
    marginTop: 5
  },
  store_down: {
    alignItems: "center"
  },
  store_down_text_view: {
    width: 45, 
    height: 22, 
    borderRadius: 10, 
    alignItems: "center",
    justifyContent: "center", 
    backgroundColor: "black"
  },
  store_down_label: {
    color: "#A8A8A8",
    fontSize: 9, 
    marginTop: 5
  },
  store_down_topic: {
    color: "#C08B65", 
    fontSize: 12
  }
})
export default AppPush