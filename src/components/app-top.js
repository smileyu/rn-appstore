import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import moment from 'moment'
const image = require('../assets/image/swiperItem.png')
const AppTop = ({item}) => {
  return <>
    <View style={styles.app_top}>
      {/* 日期📅 */}
      <View>
        <Text style={styles.app_today_chinese_text}>{moment().format("M月DD日 ddd")}</Text>
        <Text style={styles.app_today_text}>{item.topic}</Text>
      </View>
      {/* 头像👮‍♀️*/}
      <Image source={image} style={styles.app_today_image} />
    </View>
  </>
}
const styles = StyleSheet.create({
  app_top: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  app_today_chinese_text: {
  },
  app_today_text: {
    fontSize: 28,
    marginVertical: 5,
    fontWeight: "bold"
  },
  app_today_image: {
    backgroundColor: "red",
    height: 36,
    width: 36,
    borderRadius: 17
  }
})
export default AppTop