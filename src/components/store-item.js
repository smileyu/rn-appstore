// import React from 'react'
// import { View, Text, StyleSheet, FlatList } from 'react-native'
// import AppPush from '../components/app-push'
// import AppLine from '../components/app-line'

// const data = [
//   {id: 1, label: "极致体验", topic: "INCH 相机", labeColor: "", topicColor: "", image: require('../assets/image/CHIC.png')},
//   {id: 2, label: "极间相机，真实胶片🎞️", topic: "NO MO 相机 - 你的拍立得", labeColor: "", topicColor: "", image: require('../assets/image/NOMO.png')},
//   {id: 3, label: "真胶片，不浮夸", topic: "做后一卷胶片", labeColor: "", topicColor: "", image: require('../assets/image/DAZZ.png')},
//   {id: 4, label: "还原真实胶卷体验", topic: "FIMO - 复古胶片相机", labeColor: "", topicColor: "", image: require('../assets/image/OO.png')}
// ]

// const label = () => {
//   return <View style={styles.store_item}>
//     <Text style={{fontSize: 20, fontWeight: "bold"}}>热门App</Text>
//     <Text style={{color: "blue"}}>查看全部</Text>
//   </View>
// }

// const style = {
//   marginLeft: 60
// }

// const StoreItem = () => {
//   return <>
//     <FlatList data={data}
//       ListHeaderComponent={() => label()} 
//       ItemSeparatorComponent={() => <AppLine style={style}/>}
//       renderItem={({item}) => <>
//         <AppPush item={item} />
//         </>
//       }
//       keyExtractor={(item) => `${item.id}`} />
//   </>
// }
// const styles = StyleSheet.create({
//   store_item: {
//     justifyContent: "space-between",
//     flexDirection: "row",
//     alignItems: "flex-end",
//     paddingVertical: 10
//   },
//   border_line: {
//     width:"100%", 
//     height: 0.5, 
//     backgroundColor: "#A8A8A8",
//     marginLeft: 60
//   }
// })
// export default StoreItem

// import React, { Component } from 'react';
// import { Modal, Text, TouchableHighlight, View } from 'react-native';

// export default class ModalExample extends Component {

//   constructor(props) {
//     super(props);
//     this.state = {modalVisible: false};
//   }

//   setModalVisible(visible) {
//     this.setState({modalVisible: visible});
//   }

//   render() {
//     return (
//       <View style={{marginTop: 22}}>
//         <Modal
//           animationType={"slide"}
//           transparent={false}
//           visible={this.state.modalVisible}
//           onRequestClose={() => {alert("Modal has been closed.")}}
//           >
//          <View style={{marginTop: 22}}>
//           <View>
//             <Text>Hello World!</Text>

//             <TouchableHighlight onPress={() => {
//               this.setModalVisible(!this.state.modalVisible)
//             }}>
//               <Text>Hide Modal</Text>
//             </TouchableHighlight>

//           </View>
//          </View>
//         </Modal>

//         <TouchableHighlight onPress={() => {
//           this.setModalVisible(true)
//         }}>
//           <Text>Show Modal</Text>
//         </TouchableHighlight>

//       </View>
//     );
//   }
// }

import * as React from 'react';
import {
  Text,
  View,
  Image,
  Easing,
  Animated,
  StyleSheet,
  ScrollView,
  Dimensions,
  SafeAreaView,
  Platform,
  TouchableWithoutFeedback,
} from 'react-native';

const Images = [
  require('./../assets/image/app1.png'),
  require('./../assets/image/app4.png'),
  require('./../assets/image/swiperItem.png'),
  require('./../assets/image/app1.png'),
  require('./../assets/image/app1.png'),
];

// TODO: 增加详情页下拉自动关闭功能
// TODO: 图片增加阴影效果
const AppStyle = StyleSheet.create({
  imageContainer: {
    padding: 15,
    ...Platform.select({
      android: {
        elevation: 10,
      },
      ios: {
        shadowColor: '#222',
        shadowOffset: { width: 0, height: 7.5 },
        shadowRadius: 10,
        shadowOpacity: 0.5,
      }
    }),
  }
});

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.imageRef = [];
    this.oldPosotion = {};
    // 描述图片的位置变化
    this.position = new Animated.ValueXY();
    // 描述图片的大小变化
    this.measure = new Animated.ValueXY();
    // 描述图片卡片的宽、高变化值
    this.cardHeight = new Animated.Value(SCREEN_HEIGHT - 500);
    this.cardWidth = new Animated.Value(SCREEN_WIDTH);
    // 其他动画公用变量
    this.animation = new Animated.Value(0);
    this.state = {
      activeImage: null,
    }
  }

  openImage = (index) => {
    this.imageRef[index].measure((x, y, width, height, pageX, pageY) => {
      // 记录图片点击的时候位置，关闭详情视图的时候需要还原回去
      this.oldPosotion = {
        width,
        height,
        x: pageX,
        y: pageY,
      };

      // 初始化动画变量，准备开始动画
      this.position.setValue({
        x: pageX,
        y: pageY,
      });
      this.measure.setValue({
        x: width,
        y: height,
      });

      this.setState(() => {
        return {
          activeImage: Images[index],
        }
      }, () => {
        this.imageContainer.measure((x, y, width, height, pageX, pageY) => {
          Animated.parallel([
            Animated.timing(this.position.x, {
              toValue: pageX,
              // 增加一个弹性效果
              easing: Easing.back(1),
              duration: 350,
            }),
            Animated.timing(this.position.y, {
              toValue: pageY,
              easing: Easing.back(1),
              duration: 350,
            }),
            Animated.timing(this.measure.x, {
              toValue: width,
              easing: Easing.back(1),
              duration: 350,
            }),
            Animated.timing(this.measure.y, {
              toValue: height,
              easing: Easing.back(1),
              duration: 350,
            }),
            Animated.timing(this.cardWidth, {
              toValue: SCREEN_WIDTH,
              easing: Easing.back(1),
              duration: 350,
            }),
            Animated.timing(this.cardHeight, {
              toValue: SCREEN_HEIGHT - 500,
              easing: Easing.back(1),
              duration: 350,
            }),
            Animated.timing(this.animation, {
              toValue: 1,
              easing: Easing.back(1),
              duration: 350,
            }),
          ]).start();
        });
      });

    });
  }

  closeImage = () => {
    Animated.parallel([
      Animated.timing(this.position.x, {
        toValue: this.oldPosotion.x,
        easing: Easing.back(1),
        duration: 350,
      }),
      Animated.timing(this.position.y, {
        toValue: this.oldPosotion.y,
        easing: Easing.back(1),
        duration: 350,
      }),
      Animated.timing(this.measure.x, {
        toValue: this.oldPosotion.width,
        easing: Easing.back(1),
        duration: 350,
      }),
      Animated.timing(this.measure.y, {
        toValue: this.oldPosotion.height,
        easing: Easing.back(1),
        duration: 350,
      }),
      Animated.timing(this.cardWidth, {
        toValue: SCREEN_WIDTH,
        easing: Easing.back(1),
        duration: 350,
      }),
      Animated.timing(this.cardHeight, {
        toValue: SCREEN_HEIGHT - 150,
        easing: Easing.back(1),
        duration: 350,
      }),
      Animated.timing(this.animation, {
        toValue: 0,
        easing: Easing.back(1),
        duration: 350,
      }),
    ]).start(() => {
      this.setState(() => {
        return {
          activeImage: null
        }
      });
    });
  }

  render() {

    const imagContainerAnimatedStyle = {
      width: this.cardWidth,
      height: this.cardHeight,
    };

    const imageBorderAnimate = this.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [20, 0]
    });
    // 设置图片的动画
    const imageAnimatedStyle = {
      top: this.position.y,
      left: this.position.x,
      width: this.measure.x,
      height: this.measure.y,
      borderRadius: imageBorderAnimate,
    };

    const contentOpacityAnimate = this.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1]
    });

    const contentYAnimate = this.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [-150, 0]
    });
    // 设置内容的动画
    const contentAnimatedStyle = {
      opacity: contentOpacityAnimate,
      transform: [{
        translateY: contentYAnimate,
      }]
    }
    // 设置关闭按钮的动画
    const croseAnimatedStyle = {
      opacity: this.animation
    };

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1, paddingTop: 20, }}>
          {
            Images.map((image, index) => {
              return (
                <TouchableWithoutFeedback onPress={() => { this.openImage(index); }}>
                  <Animated.View
                    style={[
                      AppStyle.imageContainer, imagContainerAnimatedStyle
                    ]}
                  >
                    <Image
                      ref={(image) => { this.imageRef[index] = image; }}
                      source={image}
                      style={{ flex: 1, height: null, width: null, resizeMode: 'cover', borderRadius: 20, }}
                    />
                  </Animated.View>
                </TouchableWithoutFeedback>
              );
            })
          }
        </ScrollView>
        <View
          style={StyleSheet.absoluteFill}
          pointerEvents={this.state.activeImage ? 'auto': 'none'}
        >
          <View style={{ flex:2, zIndex: 1001, }} ref={(view) => { this.imageContainer = view; }}>
            <Animated.Image
              source={this.state.activeImage ? this.state.activeImage : null }
              style={[
                { top: 0, right: 0, height: null, width: null, resizeMode: 'cover' }, imageAnimatedStyle
              ]}
            />
            <TouchableWithoutFeedback
              hitSlop={{ top: 5, left: 5, bottom: 5, right: 5 }}
              onPress={this.closeImage}
            >
              <Animated.View style={[
                { position: 'absolute', right: 20, top: 30, },
                croseAnimatedStyle
              ]}>
                <Text style={{ fontSize: 20, color: '#fff' }}>X</Text>
              </Animated.View>
            </TouchableWithoutFeedback>
          </View>
          <Animated.View style={[
            { flex:1, zIndex: 1000, backgroundColor: '#fff' },
            contentAnimatedStyle
          ]}>
            <Text style={{ fontSize: 24 }}>这是图片的Title</Text>
            <Text>这是图片的内容区域</Text>
          </Animated.View>
        </View>
      </SafeAreaView>
    );
  }
}


